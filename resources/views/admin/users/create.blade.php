@extends('layouts.admin.admin')
@section('page_title', 'Create User')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


	<div class="box">
        <div class="box-header">
          <h3 class="box-title">New</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">

          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          
          <form role="form" name="createUser" id="createUser" method="post" action="{{url('admin/users')}}">
              
              {!! Form::token() !!}
              
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" value="{{old('password')}}" class="form-control" id="password" placeholder="Password">
                </div>               
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
	</div>
	<!-- /.box -->

@endsection