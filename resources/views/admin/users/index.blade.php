@extends('layouts.admin.admin')
@section('page_title', 'Users List')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
               {{Session::get('success')}}   
    </div>
  @endif

	<div class="box">
        <div class="box-header">
          <h3 class="box-title">Users</h3>
          <a href="./users/create"><button class="btn btn-primary pull-right">Create User</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>E-Mail</th>
              <th >Action</th>
            </tr>

            @foreach($users as $user)
	            <tr>
	              <td>{{ $loop->iteration }}</td>
	              <td>{{$user->name}}</td>
	              <td>
	                {{$user->email}}
	              </td>
	              <td><a href="{{url('admin/users/'.$user->id.'/edit')}}"><i class="fa fa-pencil" style="color: green"> </i></a>
	              	<a href="{{url('admin/users/'.$user->id)}}"><i class="fa fa-eye" style="color: gray"> </i></a>
	              	<a href="javascript:void(0);" onclick="$(this).find('form').submit();"><i class="fa fa-remove " style="color: red"> </i>
                  <form action="{{ url('admin/users/'.$user->id) }}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')                        
                    </form></a>

                  

                </td>
	            </tr>
            @endforeach
           
          </table>
        </div>
        {{ $users->links() }}
        <!-- /.box-body -->
	</div>
	<!-- /.box -->

@endsection