@extends('layouts.admin.admin')
@section('page_title', 'Profile')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


	<div class="box">
        <div class="box-header">
          <h3 class="box-title">{{$details->name}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>              
              <th>Name</th>
              <td>{{$details->name}}</td>
            </tr>
            <tr>
              <th>E-Mail</th>
              <td>{{$details->email}}</td>
          	</tr>
          </table>
        </div>
        
        <!-- /.box-body -->
	</div>
	<!-- /.box -->

@endsection