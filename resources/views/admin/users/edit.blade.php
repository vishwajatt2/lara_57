@extends('layouts.admin.admin')
@section('page_title', 'Users List')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


	<div class="box">
        <div class="box-header">
          <h3 class="box-title">Edit</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

          
          <form role="form" name="updateUser" id="updateUser" method="post" action="{{url('admin/users/'.$details->id)}}">
              @method('PUT')
              {!! Form::token() !!}
              <input type="hidden" name="srno" value="{{$details->id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" value="{{(old('name'))?old('name'):$details->name}}" class="form-control" id="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" value="{{(old('email'))?old('email'):$details->email}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <!-- <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" value="{{$details->password}}" class="form-control" id="password" placeholder="Password">
                </div> -->
                <!-- <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div> -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
	</div>
	<!-- /.box -->

@endsection