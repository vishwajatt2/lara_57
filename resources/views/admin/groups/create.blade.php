@extends('layouts.admin.admin')
@section('page_title', 'Groups')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
               {{Session::get('success')}}   
    </div>
  @endif
 
    <div class="box">
          <div class="box-header">
            <h3 class="box-title">New</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            
            <form role="form" name="createGroup" id="createGroup" method="post" action="{{url('admin/groups')}}">
                
               @csrf
                
                <div class="box-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Enter name">
                  </div>                             
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
          </div>
          <!-- /.box-body -->
    </div>

 

 @endsection