@extends('layouts.admin.admin')
@section('page_title', 'Groups')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
               {{Session::get('success')}}   
    </div>
  @endif

  <div class="box">
        <div class="box-header">
          <h3 class="box-title">List</h3>
          <a href="./groups/create"><button class="btn btn-primary pull-right">Create Group</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Contacts</th>
              <th >Action</th>
            </tr>
            @if(count($groups)>0)
              @foreach($groups as $group)
  	            <tr>
  	              <td>{{ $loop->iteration }}</td>
  	              <td>{{$group->name}}</td>
  	              <td>
  	                {{count($group->GroupContact)}}
  	              </td>
  	              <td><a href="{{url('admin/groups/'.$group->id.'/edit')}}"><i class="fa fa-pencil" style="color: green"> </i></a>
  	              	<!-- <a href="{{url('admin/groups/'.$group->id)}}"><i class="fa fa-eye" style="color: gray"> </i></a> -->
  	              	<a href="javascript:void(0);" onclick="if(confirm('Are you sure?')){$(this).find('form').submit();}"><i class="fa fa-remove " style="color: red"> </i>
                    <form action="{{ url('admin/groups/'.$group->id) }}" method="post">
                          {{ csrf_field() }}
                          @method('DELETE')                        
                      </form></a>               

                  </td>
  	            </tr>
              @endforeach

            @else
                <tr>
                  <td  colspan="4" style="text-align: center;">Records not found.</th>
                </tr>

            @endif
           
          </table>
        </div>
        {{ $groups->links() }}
        <!-- /.box-body -->
	</div>



 @endsection