@extends('layouts.admin.admin')
@section('page_title', 'Contacts')

@section('page_css')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('public/admin/')}}/bower_components/select2/dist/css/select2.min.css">
  <style type="text/css">
  	input[type="radio"] {
	  -webkit-appearance: checkbox; /* Chrome, Safari, Opera */
	  -moz-appearance: checkbox;    /* Firefox */
	  -ms-appearance: checkbox;     /* not currently supported */
	}
  </style>
@endsection


@section('page_js')
  <!-- Select2 -->
<script src="{{url('public/admin/')}}/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  $('.select2').select2()
</script>
@endsection


@section('content')

	@if(null !== Session::get('success'))
	    <div class="alert alert-success">
	               {{Session::get('success')}}   
	    </div>
  	@endif
  	  <div class="box">
        <div class="box-header">
          <h3 class="box-title">New</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">

          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          
          <form role="form" name="createContact" id="createContact" method="post" action="{{url('admin/contacts')}}">
              
              {!! Form::token() !!}
              
              <div class="box-body">
              	<div class="form-group">
                  <label for="send_type">I want to:</label>
                  <label class="radio-inline"><input type="radio" name="send_type" id="send_type_0" value="0" checked=""> Send Now</label>
				  <label class="radio-inline"><input type="radio" name="send_type" id="send_type_1" value="1"> Schedule</label>
                </div> 
                <div class="form-group">
                  <label for="sendFrom">From Number</label>
                  <select name="sendFrom" id="sendFrom" class="form-control select2" data-placeholder="Select a number">
                  	@foreach($fromNumbers as $fromNumber)
                      <option value="{{$fromNumber->number}}">{{$fromNumber->number}}</option>
                    @endforeach
                  </select>
                </div>    
                <!-- last name -->
                <div class="form-group">
                  <label for="contacts">To</label>
                  <select name="contacts[]" id="contacts" multiple="" class="form-control select2" data-placeholder="Select a number">
                  	@foreach($contacts as $contact)
                  		<option value="{{$contact->id}}">{{$contact->first_name.' '.$contact->last_name.' ('.$contact->phone.')'}}</option>
                    @endforeach
                  </select>
                </div> 
                <!-- email -->
                <div class="form-group">
                  <label for="email">Message</label>
                  <textarea class="form-control" ></textarea>
				  
                </div> 
                <!-- phone --> 
                <div class="form-group">
                  <label for="phone">Phone</label>
                  <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="phone" placeholder="Enter your 10 digit phone number" maxlength="10">
                </div> 
                <!-- Groups --> 
                <div class="form-group">
                  <label for="Groups">Groups</label>
                  <select name="groups[]" id="groups" multiple="" class="form-control select2" data-placeholder="Select a group">
                    <!--  -->
                  </select>

                  
                </div>                         
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
	</div>

@endsection