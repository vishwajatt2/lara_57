@extends('layouts.admin.admin')
@section('page_title', 'Contacts')

@section('page_css')

@endsection


@section('page_js')

@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
               {{Session::get('success')}}   
    </div>
  @endif

  <div class="box">
        <div class="box-header">
          <h3 class="box-title">Users</h3>
          <a href="./contacts/create"><button class="btn btn-primary pull-right">Create Contact</button></a>
          
          <a href="./contacts/import"><button class="btn btn-primary pull-right">Import</button></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>E-Mail</th>
              <th >Action</th>
            </tr>

            @foreach($contacts as $contact)
	            <tr>
	              <td>{{ $loop->iteration }}</td>
	              <td>{{$contact->first_name}}</td>
	              <td>
	                {{$contact->email}}
	              </td>
	              <td><a href="{{url('admin/contacts/'.$contact->id.'/edit')}}"><i class="fa fa-pencil" style="color: green"> </i></a>
	              	<a href="{{url('admin/contacts/'.$contact->id)}}"><i class="fa fa-eye" style="color: gray"> </i></a>
	              	<a href="javascript:void(0);" onclick="if(confirm('Are you sure?')){$(this).find('form').submit();}"><i class="fa fa-remove " style="color: red"> </i>
                  <form action="{{ url('admin/contacts/'.$contact->id) }}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')                        
                    </form></a>

                  

                </td>
	            </tr>
            @endforeach
           
          </table>
        </div>
        {{ $contacts->links() }}
        <!-- /.box-body -->
	</div>



 @endsection