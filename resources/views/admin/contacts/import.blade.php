@extends('layouts.admin.admin')
@section('page_title', 'Contacts')

@section('page_css')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('public/admin/')}}/bower_components/select2/dist/css/select2.min.css">
@endsection


@section('page_js')
  <!-- Select2 -->
<script src="{{url('public/admin/')}}/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  $('.select2').select2()
</script>
@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}   
    </div>
  @endif

  <div class="box">
        <div class="box-header">
          <h3 class="box-title">Import</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">

          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          
          <form role="form" enctype="multipart/form-data" name="formCsv" id="formCsv" method="post" action="{{url('admin/contacts/importCsv')}}">
              
              {!! Form::token() !!}
              
              <div class="box-body">  
              	<!-- Groups --> 
                <div class="form-group">
                  <label for="Groups">Groups</label>
                  <select name="groups[]" id="groups" multiple="" class="form-control select2" data-placeholder="Select a group">
                    @foreach($groups as $group)
                      <option value="{{$group->id}}">{{$group->name}}</option>
                    @endforeach
                  </select>                  
                </div>                
                <!-- file --> 
                <div class="form-group">
                  <label for="file">CSV:</label>
                  <input type="file" name="file" class="form-control" id="file">
                </div> 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
	</div>



 @endsection