@extends('layouts.admin.admin')
@section('page_title', 'Contacts')

@section('page_css')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('public/admin/')}}/bower_components/select2/dist/css/select2.min.css">
@endsection


@section('page_js')
  <!-- Select2 -->
<script src="{{url('public/admin/')}}/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
  $('.select2').select2()
</script>
@endsection


@section('content')


  @if(null !== Session::get('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}   
    </div>
  @endif

  <div class="box">
        <div class="box-header">
          <h3 class="box-title">New</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">

          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          
          <form role="form" name="createContact" id="createContact" method="post" action="{{url('admin/contacts')}}">
              
              {!! Form::token() !!}
              
              <div class="box-body">
                <div class="form-group">
                  <label for="first_name">First Name</label>
                  <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="first_name" placeholder="Enter name">
                </div>    
                <!-- last name -->
                <div class="form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" id="last_name" placeholder="Enter name">
                </div> 
                <!-- email -->
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="Enter email id">
                </div> 
                <!-- phone --> 
                <div class="form-group">
                  <label for="phone">Phone</label>
                  <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="phone" placeholder="Enter your 10 digit phone number" maxlength="10">
                </div> 
                <!-- Groups --> 
                <div class="form-group">
                  <label for="Groups">Groups</label>
                  <select name="groups[]" id="groups" multiple="" class="form-control select2" data-placeholder="Select a group">
                    @foreach($groups as $group)
                      <option value="{{$group->id}}">{{$group->name}}</option>
                    @endforeach
                  </select>

                  
                </div>                         
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
	</div>



 @endsection