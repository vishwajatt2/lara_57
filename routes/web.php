<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth', 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
	/*route for dashboard*/
	Route::resource('/', 'DashboardController')->only('index');	

	/*route for users*/
	Route::resource('users', 'UsersController');	

	/*route for contacts*/
	Route::get('contacts/import', 'ContactsController@import');	
	Route::post('contacts/importCsv', 'ContactsController@importCsv');	
	Route::resource('contacts', 'ContactsController');	

	/*route for contacts*/
	Route::resource('groups', 'GroupsController');

	Route::get('mm/single', 'SmsMmsController@singleForm');
	//Route::get('mm/single', 'SmsMmsController@groupForm');
});




