-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2019 at 12:55 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara57_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `email` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=ntrl; 1=sales',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=active; 1=inactive;2=trashed/deleted',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone`, `first_name`, `last_name`, `email`, `user_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(5, '1234546478', 'Red', 'Expert', 'test@gmail.com', 1, 0, 0, NULL, NULL),
(6, '1234567890', 'sstest', 'dev', 'admin@admin.com', 1, 0, 0, NULL, NULL),
(7, '4567891230', 'new', 'one', 'newone@gmail.com', 1, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'expt name 1', 1, 0, NULL, '2019-02-08 05:47:48'),
(4, 'expt name', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_contacts`
--

CREATE TABLE `group_contacts` (
  `group_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_contacts`
--

INSERT INTO `group_contacts` (`group_id`, `contact_id`, `created_at`) VALUES
(3, 5, '2019-02-08 16:02:25'),
(4, 5, '2019-02-08 16:02:25'),
(4, 6, '2019-02-08 16:02:37'),
(3, 7, '2019-02-27 15:44:39'),
(4, 7, '2019-02-27 15:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `outing_numbers`
--

CREATE TABLE `outing_numbers` (
  `id` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `sid` varchar(150) NOT NULL,
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=twilio',
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=active; 1=inactive',
  `updatedat` datetime NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outing_numbers`
--

INSERT INTO `outing_numbers` (`id`, `number`, `sid`, `service`, `user_id`, `status`, `updatedat`, `createdat`) VALUES
(1, '+14707779676 ', 'PN4cc46a3f96e8966533e6b4c91bbd0244', 0, 1, 0, '0000-00-00 00:00:00', '2019-03-01 05:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms_mms_init`
--

CREATE TABLE `sms_mms_init` (
  `id` int(11) NOT NULL,
  `from` varchar(15) NOT NULL,
  `to` text NOT NULL COMMENT 'id''s if contacts and groups',
  `to_type` int(11) NOT NULL DEFAULT '0' COMMENT '0=single; 1=group',
  `send_type` int(11) NOT NULL DEFAULT '0' COMMENT '0=instant; 1=schedule',
  `sms_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=sms; 1= mms',
  `schdl_start_tm` datetime NOT NULL,
  `schdl_end_tm` datetime NOT NULL,
  `recurring_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=active; 1=paused',
  `message` text NOT NULL,
  `attachement` text NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `third_parties`
--

CREATE TABLE `third_parties` (
  `id` int(11) NOT NULL,
  `name` varchar(180) NOT NULL,
  `value` varchar(180) NOT NULL,
  `service` varchar(180) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `third_parties`
--

INSERT INTO `third_parties` (`id`, `name`, `value`, `service`, `user_id`) VALUES
(1, 'account_sid', 'AC0237d8870354f4db37677cd3775fd727', 'twilio', 0),
(2, 'auth_token', '734af9c5ee521aa8ed71617d6fe4414b', 'twilio', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Expert Team', 'test@gmail.com', NULL, '$2y$10$kuFXV1n1skOdGlvX7aHo4uGjASN36g3WLZonAH/u2z7YZf9A8BMvK', 'mtBg8cAQUnRu4yFkQ8d7SKs3fYc4H5lvnNxtYzOv6ABjIXYOG9r36QRKhljR', '2019-01-16 01:52:57', '2019-01-16 01:52:57'),
(5, 'admin admin', 'admin@admin.com', NULL, '$2y$10$i99hWErjx7mfzichgRh7Jer0wMvPDH3Q3JAruV.5ZrakmwAjTB5JW', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_contacts`
--
ALTER TABLE `group_contacts`
  ADD KEY `group_contacts_ibfk_1` (`contact_id`),
  ADD KEY `group_contacts_ibfk_2` (`group_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outing_numbers`
--
ALTER TABLE `outing_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sms_mms_init`
--
ALTER TABLE `sms_mms_init`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `third_parties`
--
ALTER TABLE `third_parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `outing_numbers`
--
ALTER TABLE `outing_numbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sms_mms_init`
--
ALTER TABLE `sms_mms_init`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `third_parties`
--
ALTER TABLE `third_parties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
