<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function GroupContact(){
    	return $this->hasMany('App\GroupContact', "group_id", "id");
    }
}
