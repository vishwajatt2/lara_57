<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\GroupContact;
use App\Group;
use App\OutingNumbers;
use DB, Auth, Validator, File, Excel;

class SmsMmsController extends Controller
{
    //

    public function singleForm(){
        $user_id = Auth::user()->id;

        $fromNumbers = OutingNumbers::select("id","number","sid")->where("status",0)->get();



        $contacts = Contact::select("id","first_name","last_name","phone")->where("user_id", $user_id)->where("status",0)->get();        
    	return view("admin.mm.singleForm", compact("contacts","fromNumbers"));
    }
}
