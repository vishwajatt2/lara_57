<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\GroupContact;
use App\Group;
use DB, Auth, Validator, File, Excel;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$whereRow = "user_id = ".Auth::user()->id." AND status = 0";
        $contacts = DB::table('contacts')
                    ->where("user_id", Auth::user()->id)
                    ->where("status", 0)
                    ->paginate('10');
        return view("admin.contacts.index", compact("contacts"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = DB::table("groups")->where("user_id", Auth::user()->id)->where('status', 0)->get();

        return view("admin.contacts.create", compact("groups"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                        "first_name" => "required|max:150",
                        "last_name" => "required|max:150",
                        "email" => 'required|email|unique:contacts,email,'.Auth::user()->id,
                        "phone" => "required|numeric|min:10|unique:contacts,phone,".Auth::user()->id,
                        "groups" => "required|array|min:1",

        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $params = ["first_name" => $request->first_name, "last_name" => $request->last_name,"email" => $request->email, "phone" => $request->phone, "user_id" => Auth::user()->id];

        $contact_id = Contact::insertGetId($params);

        //$contact = DB::table("contacts")->insertGetId($params);

        foreach ($request->groups as $group) {
            
            //$GroupContact = new GroupContact();
            GroupContact::insert(["group_id" => $group, "contact_id" => $contact_id]);
        }

        return redirect("admin/contacts")->with("success", "Contact has been created successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $contact = Contact::with("GroupContact","Group")->where("id",$id)->get();

        $contact = GroupContact::with("Contact", "Group")->where("contact_id",$id)->get();

        $groups = Group::where([["user_id",Auth::user()->id], ["status",0]])->get();
        foreach ($contact as $key => $value) {
            
            $record['id'] = $value->Contact[0]->id;
            $record['first_name'] = $value->Contact[0]->first_name;
            $record['last_name'] = $value->Contact[0]->last_name;
            $record['email'] = $value->Contact[0]->email;
            $record['phone'] = $value->Contact[0    ]->phone;
            
            
            $record['groups'][] =$value->Group[0]->id;
           
            
        }

        return view("admin.contacts.edit", compact("record", "groups"));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
                        "first_name" => "required|max:150",
                        "last_name" => "required|max:150",
                        "email" => 'required|email|unique:contacts,email,'.$request->srno,
                        "phone" => "required|numeric|min:10|unique:contacts,phone,".$request->srno,
                        "groups" => "required|array|min:1",

        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $params = ["first_name" => $request->first_name, "last_name" => $request->last_name,"email" => $request->email, "phone" => $request->phone];

        $contact_id = Contact::where("id", $request->srno)->update($params);

        //$contact = DB::table("contacts")->insertGetId($params);

        GroupContact::where("contact_id", $request->srno)->delete();
        foreach ($request->groups as $group) {
            GroupContact::insert(["group_id" => $group, "contact_id" => $request->srno]);
        }

        return redirect("admin/contacts")->with("success", "Contact has been updated successfully.");



        /*echo '<pre>';
        print_r($request->all());*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::where("id", $id)->delete();
        GroupContact::where("contact_id", $id)->delete();
        return redirect()->back()->with("success", "Successfully removed from list.");
    }


    /*import csv into contact table*/

    public function import(){

        $groups = Group::where([["user_id",Auth::user()->id],["status",0]])->get();
        return view("admin.contacts.import", compact("groups"));
    }

    /*csv action*/
    public function importCsv(Request $request){
        if($request->hasFile('file'))
        {
          	$extension = File::extension($request->file->getClientOriginalName()); 

            /*check file extension*/
            if(!in_array($extension, ['csv'])){

                return redirect()->back()->withErrors(["Please select a valid CSV file."])->withInput();

            }

            /*check group validation*/
            $validator=Validator::make($request->all(),[            
                "groups" => "required|array|min:1",
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }


            $file = array_get($request->all(),'file');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/contacts'; 
            // GET THE FILE EXTENSION
            $extension = $file->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $fileName = rand(11111, 99999) . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            $upload_success = $file->move($destinationPath, $fileName); 

            //Import uploaded file to Database
    		$fileD = fopen($destinationPath.'/'.$fileName, "r");

    		$column=fgetcsv($fileD);

    		while(!feof($fileD)){
		         $rowData[]=fgetcsv($fileD);
		    }


			foreach ($rowData as $key => $value) {  
				if(empty($value[0]) || empty($value[0]) || empty($value[0]) || empty($value[0])){
					continue;
				}
			         
	           $params=['first_name'=>$value[0],
                        'last_name'=>$value[1],
                        'phone'=>$value[2],
                        'email'=>$value[3],
                        "user_id" => Auth::user()->id
	                    ];	            
	            

		        $contact_id = Contact::insertGetId($params);

		        //$contact = DB::table("contacts")->insertGetId($params);

		        foreach ($request->groups as $group) {
		            
		            //$GroupContact = new GroupContact();
		            GroupContact::insert(["group_id" => $group, "contact_id" => $contact_id]);
		        }
	        }

	         return redirect("admin/contacts")->with("success", "Contact has been imported successfully.");

            /*$tmp_url = $request->file->path();

            $file = $request->file('file');
            echo base_path('public\uploads\contacts');
            $file->move(base_path('public\uploads\contacts'), $request->file('file')->getClientOriginalName());*/

            //exit;
            //echo '<pre>';
            //print_r($request->all());
        }else{
            return redirect()->back()->withErrors(["Please select a file."])->withInput();
        }               

    }


        /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function downloadExcel($type)
    {
        $data = Item::get()->toArray();         

        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }


    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function importExcel(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]); 

        $path = $request->file('import_file')->getRealPath();

        $data = Excel::load($path)->get(); 

        if($data->count()){
            foreach ($data as $key => $value) {
                $arr[] = ['title' => $value->title, 'description' => $value->description];
            } 

            if(!empty($arr)){
                Item::insert($arr);
            }
        } 

        return back()->with('success', 'Insert Record successfully.');
    }
}
