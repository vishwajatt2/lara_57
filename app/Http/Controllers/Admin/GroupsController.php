<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use App\GroupContact;
use Auth,DB, Validator;
//use Form, Html;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::with('GroupContact')
                    ->where('user_id', Auth::user()->id)
                    ->where('status', 0)
                    ->paginate(10);        
       
        return view('admin.groups.index',compact("groups"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.groups.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

       $validator = Validator::make($request->all(),[
        "name" => 'required|unique:groups|max:80'
       ]) ;

       if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
       }
        
        $gr = DB::table("groups")
                ->insert([
                    "name" => $request->name,
                    "user_id" => Auth::user()->id
                ]);
        
        if($gr){
            return redirect("admin/groups")->with("success","Group has been listed successfully.");
        }else{
            return redirect()->back()->withErrors(["error"=>"Something going wrong"]);
        }
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::where('id',$id)->first();
        return view("admin.groups.edit", compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'    => 'required|max:180|unique:groups,name,'.$request->srno
       ]) ;

       if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
       }
        
        $gr = Group::where('id',$request->srno)
                ->update([
                    "name" => $request->name
                ])                ;
        
        if($gr){
            return redirect("admin/groups")->with("success","Group has been updated successfully.");
        }else{
            return redirect()->back()->withErrors(["error"=>"Something going wrong"]);
        }
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::where("id", $id)->delete();
        GroupContact::where("group_id", $id)->delete();

        return redirect("admin/groups")->with("success","Group has been removed from list.");
    }
}
