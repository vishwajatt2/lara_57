<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DB;
use Form, Html, Validator;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->paginate(10);
        //echo '<pre>';
        //print_r($users);exit;

        return view('admin/users/index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/users/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'name'     => 'required|string|max:255',
           'email'    => 'required|unique:users|max:180',
           'password' => 'required|max:180'
        ]);//->validate();

        if ($validator->fails()) {
            return redirect()
            			->back()
                        ->withErrors($validator)
                        ->withInput();
        }



        $pwd = Hash::make($request->password);

        $insert =  DB::table('users')
        ->insert([
        	"name"=>$request->name,
        	"email" => $request->email,
        	"password" => $pwd
        ]);
       
       if($insert){
       		return redirect('admin/users/')->with('success',"Listed successfully.");
       }else{
       		return redirect()->back()->withErrors(["error"=>"Something going wrong"]);
       }

        //echo '<pre>';
        //print_r($pwd);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = DB::table('users')->where("id", $id)->first();
        return view('admin/users/show', ["details" => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = DB::table('users')->where("id", $id)->first();
        return view('admin/users/edit', ["details" => $detail]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'name'   => 'required|string|max:255',
           'email'  => 'required|unique:users,email,'.$request->srno.'|max:180'
        ]);


        if($validator->fails()){
        	return redirect()->back()->withErrors($validator)->withInput();
        }

        $update = DB::table('users')->where('id',$request->srno)->update(["name" => $request->name, "email" => $request->email]);

       	if($update){
       		return redirect("admin/users")->with("success", "Updated successfully");
       	}else{
       		return redirect()->back()->withErrors(["errors" => "Oops! Something going wrong."])->withInput();
       	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("users")->where("id", $id)->delete();
        
        return redirect()->back()->with('success', "Deleted successfully.");
    }
}
