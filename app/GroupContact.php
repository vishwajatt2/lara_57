<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupContact extends Model
{
    
    public function Contact(){
    	return $this->hasMany('App\Contact', "id", "contact_id");
    }

    public function Group(){
    	return $this->hasMany('App\Group', "id", "group_id");
    }
}
